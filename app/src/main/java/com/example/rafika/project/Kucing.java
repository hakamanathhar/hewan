package com.example.rafika.project;

public class Kucing extends Hewan {

    public Kucing(int kaki, String alatNafas) {
        super(kaki, alatNafas);
    }

    public void print() {
        System.out.println("Kucing memiliki : " + super.getKaki() + ", Bernafas dengan : " + super.getAlatNafas());
    }

}
