package com.example.rafika.project;

public abstract class Hewan {
    protected int kaki;
    protected String alatNafas;

    public Hewan(int kaki, String alatNafas) {
        this.kaki = kaki;
        this.alatNafas = alatNafas;
    }

    public void setKaki(int kaki) {
        this.kaki = kaki;
    }

    public int getKaki() {
        return kaki;
    }

    public void setAlatNafas(String alatNafas) {
        this.alatNafas = alatNafas;
    }

    public String getAlatNafas() {
        return alatNafas;
    }

    public abstract void print();
}
